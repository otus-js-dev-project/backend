# backend

Сервис АПИ для проекта `Tasker`

## Description

Сервис предоставляет инструменты для создания и управления пользователями и задачами 
Конфигурирование осуществляется через .env файл
АПИ закрыты Basic auth, пользователь для доступа в .env
Данные хранятся в postgres

## Dependencies

- postgres
- discord

## Getting started

- `npm run start:watch`
