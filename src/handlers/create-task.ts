import {Request, Response} from 'express'
import * as Joi from "joi"

const schema = Joi.object({
    userId: Joi.number().min(1).required(),
    title: Joi.string().min(2).max(128).required(),
    description: Joi.string().min(2).max(255).required(),
    goal: Joi.number().min(1).required(),
})

export function createTask(pool: any, log: any) {
    return async (req: Request, res: Response) => {
        log.info(`[createTask]: body: ${JSON.stringify(req.body)}`)
        const validation = schema.validate(req.body);

        if (validation.error) {
            res.status(400).send({ error: validation.error});
            return;
        }

        const {userId, title, description, goal} = req.body

        try {
            const task = await pool.query('INSERT INTO tasks(user_id, title, description, goal) VALUES($1, $2, $3, $4) RETURNING task_id AS "taskId";', [userId, title, description, goal])

            res.send({taskId: task[0].taskId});
        } catch (e: any) {
            res.status(500).send({error: e.message})
        }
    }
}