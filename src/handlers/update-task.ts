import {Request, Response} from 'express'
import * as Joi from "joi"

const schema = Joi.object({
    taskId: Joi.number().min(1).required(),
    state: Joi.string().valid('CREATED', 'STARTED', 'PAUSED', 'FINISHED', 'DELETED').required(),
    timer: Joi.number().required(),
})

export function updateTask(pool: any, log: any) {
    return async (req: Request, res: Response) => {
        log.info(`[updateTask]: body: ${JSON.stringify(req.body)}`)
        const validation = schema.validate(req.body);

        if (validation.error) {
            res.status(400).send({ error: validation.error});
            return;
        }

        const {state, timer, taskId} = req.body

        try {
            const task = await pool.query('SELECT title FROM tasks WHERE task_id = $1;', [taskId])

            if (!task.length) {
                res.status(404).send({ error: `Task '${taskId}' not found` });
                return
            }

            await pool.query('UPDATE tasks SET timer = $1, state = $2 WHERE task_id = $3;', [timer, state, taskId])

            res.send({success: true});
        } catch (e: any) {
            res.status(500).send({error: e.message})
        }
    }
}