import {Request, Response} from 'express'
import * as Joi from "joi"

const schema = Joi.object({
    userId: Joi.number().min(1).required(),
})

export function getHistory(pool: any, log: any) {
    return async (req: Request, res: Response) => {
        log.info(`[getHistory]: body: ${JSON.stringify(req.body)}`)
        const validation = schema.validate(req.body);

        if (validation.error) {
            res.status(400).send({ error: validation.error});
            return;
        }

        const {userId} = req.body

        try {
            const tasks = await pool.query(`SELECT
                                                   task_id AS "taskId",
                                                   title,
                                                   description,
                                                   timer,
                                                   goal,
                                                   created_at AS "createdAt",
                                                   state
                                            FROM tasks WHERE user_id = $1 
                                            ORDER BY created_at DESC;`, [userId])

            res.send({tasks});
        } catch (e: any) {
            res.status(500).send({error: e.message})
        }
    }
}