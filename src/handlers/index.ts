import { authorization } from "./authorization"
import { registration } from "./registration"
import { createTask } from "./create-task"
import { updateTask } from "./update-task"
import { getHistory } from "./get-history"

export {
    authorization,
    registration,
    createTask,
    updateTask,
    getHistory
}