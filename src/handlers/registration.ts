import {Request, Response} from 'express'
import * as Joi from "joi"

const schema = Joi.object({
    name: Joi.string().min(2).max(64).required(),
    lastname: Joi.string().min(2).max(64).required(),
    password: Joi.string().min(2).max(64).required(),
    username: Joi.string().min(2).max(64).required(),
})

export function registration(pool: any, log: any) {
    return async (req: Request, res: Response) => {
        const {name, lastname, password, username} = req.body
        log.info(`[registration]: body: ${JSON.stringify(req.body)}`)

        const validation = schema.validate(req.body);

        if (validation.error) {
            res.status(400).send({ error: validation.error});
            return;
        }

        try {
            const user = await pool.query('INSERT INTO users(name, lastname, password, username) VALUES ($1, $2, $3, $4) RETURNING user_id AS "userId";', [name, lastname, password, username])

            res.send({userId: user[0].userId});
        } catch(e: any) {
            res.status(500).send({error: e.message})
            return
        }
    }
}