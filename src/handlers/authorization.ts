import {Request, Response} from 'express'
import * as Joi from "joi"

const schema = Joi.object({
    password: Joi.string().min(2).max(64).required(),
    username: Joi.string().min(2).max(64).required(),
})

export function authorization(pool: any, log: any) {
    return async (req: Request, res: Response) => {
        log.info(`[authorization]: body: ${JSON.stringify(req.body)}`)
        const validation = schema.validate(req.body);

        if (validation.error) {
            res.status(400).send({ error: validation.error});
            return;
        }

        const {username, password} = req.body

        try {
            const user = await pool.query('SELECT user_id AS "userId", password FROM users WHERE username = $1;', [username])

            if (!user || !user.length) {
                res.status(404).send({error: `User not found`})
                return
            }

            if (user[0].password !== password) {
                res.status(404).send({error: `Not valid password`})
                return
            }

            res.send({userId: user[0].userId});
        } catch (e: any) {
            res.status(500).send({error: e.message})
            return
        }
    }
}