import * as express from 'express'
import * as dotenv from 'dotenv'
import {Express, Request, Response} from 'express'
import * as bodyParser from 'body-parser'

dotenv.config()
import Config from "./modules/config"

import {
    authorization,
    registration,
    getHistory,
    createTask,
    updateTask,
} from "./handlers"
import { auth } from "./middleware"
import Health from "./modules/health"
import Logger from "./modules/logger/logger"
import { MyPool } from "./modules/db"

const app: Express = express()

const config = Config
const log = new Logger(config).initLogger()
const pool = new MyPool(config.db, log)
const health = new Health()

const { appPort, apiPath, apiHealthPath } = config;

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

const cors=require('cors');
app.use(cors())

app.use(bodyParser.json())
app.use(auth(config))

// core api
app.post(`${apiPath}/registration`, registration(pool, log))
app.post(`${apiPath}/authorization`, authorization(pool, log))
app.post(`${apiPath}/create-task`, createTask(pool, log))
app.post(`${apiPath}/update-task`, updateTask(pool, log))
app.post(`${apiPath}/get-history`, getHistory(pool, log))

// health api
app.get(`${apiHealthPath}/livenessProbe`, (req: Request, res: Response) => {
    res.send({text: 'healthy'})
})
app.get(`${apiHealthPath}/readinessProbe`, async (req: Request, res: Response) => {
    try {
        await health.check(pool, log)
        res.send({text: 'healthy'})
    } catch (e: any) {
        log.error(`Some sources not a live: `, e)
        res.status(503).send({test: 'unhealthy'})
    }
})

app.listen(appPort, async () => {
    await health.check(pool, log)

    log.info(`[server]: Server is running on port ${appPort}`)
})
