class Config {
    readonly appPort = process.env.APP_PORT || '8080'
    readonly logLevel = process.env.LOG_LEVEL || 'trace'
    readonly apiPath = process.env.API_PATH || '/api/v1'
    readonly apiHealthPath = process.env.API_HEALTH_PATH || '/api/health'
    readonly username = process.env.AUTH_USER
    readonly password = process.env.AUTH_PASSWORD
    readonly discord = new Discord()
    readonly db = new DB()
}

class Discord {
    readonly discordLogLevel = process.env.DISCORD_LOG_LEVEL || 'fatal'
    readonly enable = process.env.DISCORD_ENABLE ? JSON.parse(<string>process.env.DISCORD_ENABLE) : true
    readonly url = process.env.DISCORD_URL || 'https://discord.com/api/webhooks/849313297165844510/PL1AayOnFCKjuqQJ5YLUv0YudvAO8CfUGCvFlCJpCiKtZ46TtAH7m_RB-CXoKQegpBrS'
    readonly name = process.env.DISCORD_BOT_NAME || 'DevelopChat'
}

class DB {
    readonly host = process.env.DB_HOST || 'localhost'
    readonly port = process.env.DB_PORT || '5432'
    readonly user = process.env.DB_USER || 'postgres'
    readonly password = process.env.DB_PASSWORD || ''
    readonly database = process.env.DB_DATABASE || 'tasker'
    readonly schema = process.env.DB_SCHEMA || 'tasker'
}

export default new Config()