import axios from 'axios'

export default async function SendToDiscord (config: any, content: string): Promise<void> {
    if (!config.enable) {
        return
    }

    try {
        const res = await axios.post(config.url, {
            username: config.name,
            content,
        })

        if (res.status === 200 || res.status === 204) {
            return
        }

        console.error(res)
    } catch (err) {
        console.error('Discord Dev Error', err)
    }
}
