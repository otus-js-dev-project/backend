import pino from 'pino'
import SendToDiscord from "./discord"

export default class Logger {
  config: any
  pinoLogger: any

  constructor(config: any) {
    this.config = config
    this.pinoLogger = pino({
      transport: {
        target: 'pino-pretty',
        options: {
          colorize: true,
        },
      },
      level: this.config.logLevel,
      hooks: {
        logMethod(inputArgs: any, method: any, level: number) {
          if (level >= logLevelToNumber(config.discord.discordLogLevel)) {
            SendToDiscord(config.discord, JSON.stringify(inputArgs))
                .then(() => {})
                .catch((e: any) => console.log('Discord send error ', e))
          }
          return method.apply(this, inputArgs);
        }
      }
    })
  }

  initLogger() {
    return this.pinoLogger
  }
}

function logLevelToNumber(level: number): number {
  const levels = {
    trace: 10,
    debug: 20,
    info: 30,
    warn: 40,
    error: 50,
    fatal: 60
  }

  return levels[level as unknown as keyof typeof levels] || 60
}
