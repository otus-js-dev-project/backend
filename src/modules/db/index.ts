import * as Pool from 'pg-pool'

export class MyPool {
    config: any
    log: any
    pool: any
    client: any

    constructor(config: any, log: any) {
        this.config = config
        this.pool = new Pool({
            database: config.database,
            user: config.user,
            password: config.password,
            port: config.port,
        })
        this.log = log
    }

    async getConnect(): Promise<any> {
        if (this.client) {
            return this.client
        }

        try {
            this.client = await this.pool.connect()
        } catch (e: any) {
            this.log.error(`[db]: Get connect error. `, e)
        }
    }

    async testQuery() {
        let connect: any = null

        if (this.pool) {
            try {
                connect = await this.pool.connect()
                await connect.query(`SELECT NOW();`)
                return
            } catch (e: any) {
                this.log.error(`[db]: Test query error. `, e)
            } finally {
                if (connect) {
                    connect.release()
                }
            }
        }

        throw new Error(`Connection pool to DB not established`)
    }

    async query(query: string, params: Array<any>): Promise<any[]> {
        let connect: any = null
        this.log.debug(`[db]: query: '${query.substring(0, 20)}...' with params: ${JSON.stringify(params)}`)

        if (this.pool) {
            try {
                connect = await this.pool.connect()
                const {rows} = await connect.query(query, params)
                this.log.debug(`[db]: result: ${JSON.stringify(rows)}`)

                return rows
            } catch (e: any) {
                this.log.error(`[db]: Query error: `, e)
                throw new Error(e)
            }  finally {
                if (connect) {
                    connect.release()
                }
            }
        }

        throw new Error(`Connection pool to DB not established`)
    }
}