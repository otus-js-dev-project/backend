export default class Health {
    constructor() {
    }

    async check(pool: any, log: any) {
        try {
            await pool.query('SELECT NOW()', [])
        } catch (e: any) {
            log.fatal(`Error pool checking: ${e}`)
            throw new Error('Pool error')
        }
    }
}