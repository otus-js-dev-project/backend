## API

Программный интерфейс приложения

- [API](#api)
  - [Methods](#methods)
    - [registration](#registration)
      - [Input](#input)
      - [Output](#output)
    - [authorization](#authorization)
      - [Input](#input)
      - [Output](#output)
    - [create-task](#createtask)
      - [Input](#input)
      - [Output](#output)
    - [update-task](#updatetask)
      - [Input](#input)
      - [Output](#output)    
    - [get-history](#gethistory)
      - [Input](#input)
      - [Output](#output)    


#### Methods

Обработчики

##### registration

POST /registration

Регистрация нового пользователя

###### Input

Принимает параметры

- `name` - *string* - имя
- `lastname` - *string* - фамилия
- `password` - *string* - пароль
- `username` - *string* - имя пользователя
  
###### Output

Возвращает параметры

- `userId` - *number* - ид. нового пользователя

##### authorization

POST /authorization

Проверяет наличие пользователя в БД

###### Input

Принимает параметры

- `password` - *string* - пароль
- `username` - *string* - имя пользователя

###### Output

- `userId` - *number* - ид. нового пользователя

##### createtask

POST /create-task

Добавляет зрителя

###### Input

Принимает параметры

- `userId` - *number* - ид. пользователя
- `title` - *string* - название
- `description` - *string* - описание
- `goal` - *number* - цель в минутах
  
###### Output

Возвращает параметры

- No

##### updatetask

POST /update-task

Добавляет задачу и связывает ее с переданными зрителями

###### Input

Принимает параметры

- `taskId` - *number* - ид. задачи
- `state` - *string* - статус задачи (может принимать значения: 'CREATED', 'STARTED', 'PAUSED', 'FINISHED', 'DELETED')
- `timer` - *number* - затраченное время в секундах

###### Output

- "success": true

##### gethistory

POST /get-history

Удаляет пользователя и его связи с задачей

###### Input

Принимает параметры

- `userId` - *number* - ид. пользователя

###### Output

Массив объектов с параметрами:
- `taskId` - *number* - ид. задачи
- `title` - *string* - название
- `description` - *string* - описание
- `timer` - *number* - затраченное время в секундах
- `goal` - *number* - цель в минутах
- `state` - *string* - статус задачи (может принимать значения: 'CREATED', 'STARTED', 'PAUSED', 'FINISHED', 'DELETED')
