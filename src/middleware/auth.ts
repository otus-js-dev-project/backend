import {NextFunction, Request, Response} from "express";
import * as basicAuth from "basic-auth";

export function auth(config: any) {
    const { username, password } = config

    return (req: Request, res: Response, next: NextFunction) => {
        const urlElements = req.url.split('/')
        const methodName = urlElements[urlElements.length - 1]

        if (methodName === 'livenessProbe' || methodName === 'readinessProbe') {
            next()
            return
        }
        // check auth by .env__
        const user: any = basicAuth(req)
        if (!user || user.name !== username || user.pass !== password) {
            res.status(403).send({error: `Access denied`})
            return
        }
        // TODO schema validation
        next()
    }
}