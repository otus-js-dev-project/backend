FROM node:16-alpine as backend

WORKDIR /app
#system
RUN apk add --no-cache tzdata;
###Added user sensei and use it###
RUN mkdir -p /app/public
RUN chown -R node: /app
RUN npm install -g typescript
RUN apk update && apk add bash py-pip make gcc && rm -rf /var/cache/apk/*
USER node
COPY --chown=node:node package*.json ./
RUN npm install

COPY --chown=node:node . /app

RUN npm run build

EXPOSE 8088
CMD [ "node", "--max-old-space-size=2048", "-r", "ts-node/register/transpile-only", "-r", "tsconfig-paths/register", "./dist/index.js" ]
